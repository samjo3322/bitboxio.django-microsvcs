# README #
This is a working project to research a micro-service architecture using Django.
- https://pypi.org/project/django-microservices/

### Specifications ###
* Python 3.6.6
* Django + Celery + RabbitMQ
* Reference - https://simpleisbetterthancomplex.com/tutorial/2017/08/20/how-to-use-celery-with-django.html


### Python Virtual Environment ###
* Activate Python VENV
$ python3 -m venv ~/bitboxio.django-microsvcs
$ source ~/bitboxio.django-microsvcs/bin/activate
$ deactivate

### Installation ###
$ brew install rabbitmq
$ pip install -r requirements.txt

### Run Locally ###
```bash
python manage.py migrate
```

```bash
python manage.py runserver
```

```bash
celery -A mysite worker -l info
```

Make sure you have RabbitMQ service running.

```bash
rabbitmq-server
```
